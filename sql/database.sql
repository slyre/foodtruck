PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT, created_date TIME, last_name TEXT NOT NULL DEFAULT '', first_name TEXT NOT NULL DEFAULT '', description TEXT NOT NULL DEFAULT '', image TEXT NOT NULL DEFAULT '', id_description INTEGER DEFAULT 'TEXT ' NOT NULL);
INSERT INTO user VALUES(1,'2019-11-02 14:04:00','','Claude','Claude s''occupe de la cuisine pour vous fournir de bons petits plâts, rapides à manger selon vos envies.','image1_equipe.jpg','description_claude');
INSERT INTO user VALUES(2,'2019-11-02 14:04:00','','Marc','Marc s''occupe de votre satisfaction','image3_equipe.jpg','description_marc');
INSERT INTO user VALUES(3,'2019-11-02 14:04:00','','Laura','Laura s''occupe du sevirce et de l''encaissement','images/image2_equipe.jpg','description_laura');
CREATE TABLE IF NOT EXISTS "product" (
	id INTEGER,
	created_date TIME,
	content TEXT,
	name TEXT
, "type" TEXT);
INSERT INTO product VALUES(5,'2019-02-13 10:35:00','Salade verte, lardons, croûtons, oeuf dur, dès d''emmental, vinaigrette.','Lyonnaise.','salade');
INSERT INTO product VALUES(6,'2019-02-13 10:35:00','Salade verte, chèvre chaud pané, croûtons, maïs, tomate, noix, vinaigrette.','Biquette.','salade');
INSERT INTO product VALUES(7,'2019-02-13 10:35:00','Salade verte, thon, anchois, tomate, olives noires, poivrons, oeuf dur, vinaigrette.','Niçoise.','salade');
INSERT INTO product VALUES(12,'2019-02-13 10:35:00','Vanille, fraise ou chocolat.','Glace à l''italienne.','dessert');
INSERT INTO product VALUES(13,'2019-02-13 10:35:00','Moelleux au chocolat.','Moelleux au chocolat.','dessert');
INSERT INTO product VALUES(14,'2019-02-13 10:35:00','Sucre, pâte à tartiner, confiture.','Crêpe.','dessert');
INSERT INTO product VALUES(15,'2019-02-13 10:35:00','Eau gazeuse','Perrier.','boisson');
INSERT INTO product VALUES(16,'2019-02-13 10:35:00','Pêche ou Mangue','Ice tea.','boisson');
INSERT INTO product VALUES(1,'2019-02-13 11:16:20','Pain maison, steak haché, baccon, oignon, cheddar, salade, tomate, mayonnaise.','Cheese burger.','burger');
INSERT INTO product VALUES(2,'2019-02-13 11:17:15','Pain maison, galette de légumes, oignon, cheddar, salade, tomate, sauce ranch.','Végé.','burger');
INSERT INTO product VALUES(3,'2019-02-13 11:17:21','Pain maison, steak haché, oignon, chèvre, salade, tomate, sauce au miel.','Seguin.','burger');
INSERT INTO product VALUES(4,'2019-02-13 11:17:26','Pain maison, filet de poulet pané, oignon, cheddar, salade, tomate, sauce BBQ.','Poulette.','burger');
INSERT INTO product VALUES(8,'2019-02-13 11:17:32','Sauce tomate, mozzarella, olives, origan.','Marguerita','pizza');
INSERT INTO product VALUES(9,'2019-02-13 11:17:43','Sauce tomate, mozzarella, bleu, chèvre, raclette, olives, origan.','4 Fromages.','pizza');
INSERT INTO product VALUES(10,'2019-02-13 11:18:06','Crème, mozzarella, champignons, persillade, olives, origan.','Forestière.','pizza');
INSERT INTO product VALUES(11,'2019-02-13 11:18:12','Sauce tomate, mozzarella, artichauts, tomates fraîches, champignons, olives, origan.','Végétarienne.','pizza');
INSERT INTO product VALUES(17,'2019-02-13 11:31:56','Cherry, Zero, Light ou Classique','Coca-Cola.','boisson');
INSERT INTO product VALUES(18,'2019-02-13 11:32:23','Limonade','Sprite.','boisson');
CREATE TABLE post (id, created_date, title, content, image);
INSERT INTO post VALUES(1,'2019-02-13 11:42:12','C est la Chandler','Durant le mois de Février, venez goûter chez nous! Nous vous préparons de délicieuses crêpes pour vous retrouver entre Friends!!','https://pbs.twimg.com/profile_images/917789882776522752/7nZeyOPc_400x400.jpg');
INSERT INTO post VALUES(2,'2019-02-13 11:54:01','Carnaval!','Pour toute personne déguisée entre le 1er et le 28 Février 2019, à l achat d un menu, le second est à moitié prix! Consultez la page Nos produits pour découvrir nos délicieuses formule durant le mois de Février, venez goûter chez nous! Nous vous préparons de délicieuses crêpes pour vous retrouver entre Friends!!','https://cache.cosmopolitan.fr/data/photo/w1000_ci/52/ted-et-la-sluty-pumkin-himym.jpg');
INSERT INTO post VALUES(3,'2019-02-13 11:56:53','Winter is coming!','Sous une petite tonnelle chauffée, dégustez nos merveilleux plats en toute convivialité.','http://www.objectifgard.com/wp-content/uploads/2016/12/winter-is-coming-1050x600.jpeg');
DELETE FROM sqlite_sequence;
INSERT INTO sqlite_sequence VALUES('user',3);
INSERT INTO sqlite_sequence VALUES('product',18);
CREATE TABLE contact (id INTEGER PRIMARY KEY AUTOINCREMENT, name, email, subject, content);
COMMIT;
