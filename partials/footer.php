
       
       <div class="plan_du_site">
            <h3>Plan du site</h3>
            <ul>
                <li><a href="actus.php">Actualités</a></li>
                <li><a href="page_produits.php">Nos Produits</a></li>
                <li><a href="equipe.php">Equipe</a></li>
                <li><a href="contact.php">Contact</a></li>
             </ul>
        </div>

        <div class="suivez_nous">
            <h3>Nous contacter</h3>
            <p id=numero>N° 04.79.79.79.78</p>
            <ul class="liste_logos">
                <li class="logos"><a href="https://www.facebook.com" id="logo_facebook"><i class="fab fa-facebook-square"></i></a></li>
                <li class="logos"><a href="https://www.twitter.com" id="logo_twitter"><i class="fab fa-twitter-square"></i></a></li>
                <li class="logos"><a href="https://www.instagram.com" id="logo_instagram"><i class="fab fa-instagram"></i></a></li> <!--L'icone instagram n'existe pas en "square" sur le site :( -->
                <li class="logos"><a href="https://pinterest.com" id="logo_pinterest"><i class="fab fa-pinterest-square"></i></a></li>
                <li class="logos"><a href="https://www.youtube.com" id="logo_youtube"><i class="fab fa-youtube-square"></i></a></li>
            </ul>

            <p>Copyright 2019.</p>

        </div>    

