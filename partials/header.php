        <div class="menu_de_navigation">
            <nav>
                <ul class="menu">
                    <li><a href="actus.php"><i class="fas fa-home"></i>  Actualités</a></li> <!--icone maison-->
                    <li><a href="page_produits.php"><i class="fas fa-utensils"></i>  Nos Produits</a></li> <!--icone fourchette-->
                    <li><a href="equipe.php"><i class="fas fa-user-friends"></i>  Equipe</a></li> <!--icone equipe-->
                    <li><a href="contact.php"><i class="fas fa-envelope"></i>  Contact</a></li> <!--icone enveloppe-->
                </ul>
            </nav>
        </div>    
