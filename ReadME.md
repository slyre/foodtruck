## Comment actualiser un fichier sur git :

1. ``` git pull ``` (**à faire en premier, sinon on ne pourra pas push à la fin**)
2. ``` git add nomdufichier/dossier```
3. ``` git commit -m "commentaire" ```
4. ``` git push ```

## Comment récupérer notre site et accéder à celui-ci?

1. Ouvrir le terminal
2. ```git clone https://gitlab.com/nacsimplon/foodtruck.git```
3. ```cd foodtruck```
4. ```sqlite3 database.db < ./sql/database.sql```
5. ```php -S localhost:8000```
6. ```localhost:8000/web/actus.php```
