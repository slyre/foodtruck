
let diapoIndexCourant = 0;

window.onload = function () {
    //dom not only ready, but everything is loaded
    glisse(diapoIndexCourant);
};



function glisseplusun(valeur) {
    glisse(diapoIndexCourant += valeur);
}

function glisse(nouvelIndex) {
    

    // Images diapo est un array
    let imagesDiapo = document.getElementsByClassName("banactu");

    // Ex : tableau avec 2 éléments 
    let nbImages = imagesDiapo.length;

    // Le compteur dépasse la taille du tableau (nouvelIndex = 2) => on reset le compteur à 0
    if (nouvelIndex == nbImages) {
        diapoIndexCourant = 0;
    }

    // Si le nouvel index est -1 , le nouvel index sera le dernier élément du tableau 
    if (nouvelIndex < 0) {
        diapoIndexCourant = nbImages - 1;
    }

    // Cache toutes les photos
    for (let i = 0; i < nbImages; i++) {
        imagesDiapo[i].style.display = "none";
    }

    // Affiche la photo courante
    imagesDiapo[diapoIndexCourant].style.display = "initial";
}



