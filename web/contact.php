<?php 
$db = new SQLite3('../database.db');
?>

<!doctype html>
<html lang="fr">

<head>
    <title>Contact</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/feuille_de_style_commun.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
        crossorigin="anonymous">
    <link rel="stylesheet" href="css/contact.css">
    <link href="https://fonts.googleapis.com/css?family=Noticia+Text" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Martel+Sans" rel="stylesheet">

</head>

<body>
    <header role="navigation">
        <!-- menu de la barre de nav -->

        <?php
            include('../partials/header.php');
            
             ?>
    </header>

    <div role="banner" class="banniere_contact">
        <img src="images/banniere_contact.jpg" alt="nourriture de type fast-food">
    </div>

    <div class="rectangle_vert">
        <p class="contact">contact</p>
    </div>

    <article role="main" class="formulaire">
        <h4 id="envoyez"> Envoyez nous un message !</h4>
        

        <!-- ce php sert à envoyer le contenu du formulaire dans la base de données -->

        <?php

	  if(isset($_POST['formulaireEnvoyer']))
	  {
        
        $varNom = $_POST['formulaireNom'];
        $varEmail = $_POST['formulaireEmail'];
        $varSujet = $_POST['formulaireSujet'];
        $varMessage = $_POST['formulaireMessage'];
        // sert à envoyer les infos dans la base de données 
        $db->exec("INSERT INTO contact (name, email, subject,content) VALUES ('$varNom', '$varEmail','$varSujet','$varMessage')");
      }
      
     ?>
        <!-- formulaire avec les différentes catégories à envoyer et la carte avec sa div et sa classe -->
        <form action="contact.php" method="post">
            <div class="carte">
                <iframe width="600" height="450" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJ9fRNJH2oi0cRrvJn2SneI4o&key=AIzaSyAWpFW57I-nYLxzVHo9Hmp_Skv4ToXYlcs"
                    allowfullscreen></iframe>
            </div>
            <p>
                Votre Nom
                <br>
                <!-- chaque input sert à mettre le format questionnaire et sa taille-->
                <input required="required" type="text" name="formulaireNom" placeholder="Quel est votre nom ?" size="20%"
                    maxlength="20" />

            </p>

            <p> Votre e-mail
                <br>
                <input required="required" type="email" name="formulaireEmail" placeholder="Quelle est votre adresse e-mail ?"
                    size="20%" maxlength="45" />
            </p>

            <p> Sujet
                <br>
                <input required="required" type="text" name="formulaireSujet" placeholder="Auquel sujet nous contactez-vous ?"
                    size="20%" maxlength="20" />
            </p>

            <p> Votre message
                <br>
                <textarea required="required" class="taille_zone" name="formulaireMessage" cols="80" rows="10"></textarea>
            </p>
            <br>
            <span>
                <input name="formulaireEnvoyer" class="envoyercss" type="submit" value="Envoyer" />
            </span>


        </form>

    </article>

    <footer role="contentinfo">

        <?php
            include('../partials/footer.php');
             ?>
    </footer>

</body>

</html>