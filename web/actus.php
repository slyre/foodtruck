<?php
    $selection_bd = new SQLite3('../database.db');
    $resultat_requete = $selection_bd->query('SELECT * FROM post');
?>


<!DOCTYPE html>
<html lang=fr>
    <head>
        <title>Actualités</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/actus.css">
        <link rel="stylesheet" href="css/feuille_de_style_commun.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Indie+Flower">
        <link href="https://fonts.googleapis.com/css?family=Noticia+Text" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/css?family=Martel+Sans" rel="stylesheet"> 
        <script type = "text/javascript" src = "javascript/actus.js"></script>
    </head>
        
    <body>

        <header role="navigation">
            <?php
            include('../partials/header.php');
            ?>
        </header>

            <div class="banniere_animee" role="banner">
                <button class = "bouton_droit" onclick = "glisseplusun(1)">&#8250;</button>
                <button class = "bouton_gauche" onclick = "glisseplusun(-1)">&#8249;</button>
                <img  class="banactu" src="images/banniere_actus.jpg" alt="Le FoodTruck!" title="C'est le Food!">
                <img  class="banactu" class="images_cachees" src="images/ban1.jpg" alt="La Pizza" title="La Pizza!">
                <img  class="banactu" class="images_cachees" src="images/banniere_contact.jpg" alt="différents type de nourriture fast-food" title="fast-food">
                <img  class="banactu" class="images_cachees" src="images/banniere_equipe.jpg" alt="Membres de l'équipe" title="Equipe">


            </div>

            <div class ="rectangle_vert">
                <p class="evenements">EVENEMENTS</p>
            </div>

            <div class="liste_evenements" role="main">
                <?php while ($contenu = $resultat_requete->fetchArray()):?>
                    <article class="chandeleur">
                        <div class="gauche">
                            <h2 class="titre_evenements"><?php echo $contenu['title']?></h2>
                            <p><?php echo$contenu['content']?></p>
                        </div>
                        <div class="droite">
                            <img src="<?php echo $contenu['image']?>" alt="<?php echo $contenu['title']?>" title="<?php echo $contenu['title']?>">
                        </div>
                    </article>
                <?php endwhile;?>
            </div>
        
        <footer role="contentinfo">
                <?php
                include('../partials/footer.php');
                ?>  
        </footer>

    </body>
    
    
</html>