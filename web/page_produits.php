<?php 
$db = new SQLite3('../database.db');

$requeteTousLesProduits = "SELECT * FROM product";
$requeteTousLesBurgers = 'SELECT * FROM product WHERE type="burger"';
$requeteToutesLesSalades = 'SELECT * FROM product WHERE type="salade"';
$requeteToutesLesPizzas = 'SELECT * FROM product WHERE type="pizza"';
$requeteTousLesDesserts = 'SELECT * FROM product WHERE type="dessert"';
$requeteToutesLesBoissons = 'SELECT * FROM product WHERE type="boisson"';
$fullrequest = $db->query($requeteTousLesProduits);
$requeteburger = $db->query($requeteTousLesBurgers);
$requetesalade = $db->query($requeteToutesLesSalades);
$requetepizza = $db->query($requeteToutesLesPizzas);
$requetedessert = $db->query($requeteTousLesDesserts);
$requeteboisson = $db->query($requeteToutesLesBoissons);
?>

<!DOCTYPE html>

<html lang="fr">

<head>
    <meta charset="utf-8" />
    <title>Page produits</title>

    <!--Lien vers fichier css pour les parties communes-->
    <link rel="stylesheet" href="css/feuille_de_style_commun.css">

    <!--Lien vers fontawesome.com pour récupérer les icones-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!--Lien vers google fonts.com pour récupérer les polices "Noticia Text" et "Martel Sans-->
    <link href="https://fonts.googleapis.com/css?family=Noticia+Text" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Martel+Sans" rel="stylesheet">

    <!--Lien vers fichier css pour ma page produits-->
    <link rel="stylesheet" href="css/page_produits.css">


</head>


<body>

    <header role="navigation">

        <?php
            include('../partials/header.php');
             ?>

    </header>

    <div class="ban_produits" role="banner">
        <img src="images/ban1.jpg" alt="pizza sur une planche" />
    </div>





    <div class="rectangle_vert">
        <p class="plats">Plats</p>
    </div>

    <article role="main">

        <section class="liste_des_plats">

            <div class="burgers">
                <h2>Burgers</h2>
                <ul>

                    <?php
 
                     while($resultatburger = $requeteburger->fetchArray(SQLITE3_ASSOC)){ 
 
                     if(!isset($resultatburger['name'])) continue; 
 
                        echo "<li><strong> {$resultatburger["name"]} </strong> {$resultatburger["content"]} </li>" ;
                         }
                        
                    ?>
                </ul>
            </div>

            <div class="salades">
                <h2>Salades</h2>
                <ul>
                    <?php
                    while($resultatsalade = $requetesalade->fetchArray(SQLITE3_ASSOC)){ 

                        if(!isset($resultatsalade['name'])) continue;   

                            echo "<li><strong> {$resultatsalade["name"]} </strong> {$resultatsalade["content"]} </li>" ;
                    }
                    ?>
                </ul>
            </div>

            <div class="pizzas">
                <h2>Pizzas</h2>

                <ul>
                    <?php 
						
						while($resultatpizza = $requetepizza->fetchArray(SQLITE3_ASSOC)){ 

							if(!isset($resultatpizza['name'])) continue; 
							
							echo "<li><strong> {$resultatpizza["name"]} </strong> {$resultatpizza["content"]} </li>" ;
                        }
					    ?>

                </ul>
            </div>

            <div class="vide">
            </div>

        </section>


        <div class="rectangle_vert">
            <p class="plats">Desserts & Boissons</p>
        </div>
        <section class="desserts_et_boissons">

            <div class="desserts">
                <h2>Desserts</h2>

                <ul>
                    <?php
                    while($resultatdessert = $requetedessert->fetchArray(SQLITE3_ASSOC)){ 

                    if(!isset($resultatdessert['name'])) continue; 

                    echo "<li><strong> {$resultatdessert["name"]} </strong> {$resultatdessert["content"]} </li>" ;
                    }
                    ?>
                </ul>
            </div>

            <div class="boissons">
                <h2>Boissons</h2>
                <ul>
                    <?php

                    while($resultatboisson = $requeteboisson->fetchArray(SQLITE3_ASSOC)){ 

                    if(!isset($resultatboisson['name'])) continue; 

                    echo "<li><strong> {$resultatboisson["name"]}</strong> {$resultatboisson["content"]}</li>";
                    }
                    ?>
                </ul>
            </div>

        </section>

    </article>



    <footer role ="contentinfo">
        <?php
            include('../partials/footer.php');
             ?>
    </footer>

</body>

</html>