<?php
class MyDB extends SQLite3
{
    function __construct()
    {
        $this->open('../database.db');
    }
}
    $db = new MyDB();
    $result = $db->query('SELECT * FROM user');
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <title>Equipe Du FoodTruck</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/feuille_de_style_commun.css">
    <link rel="stylesheet" href="css/equipe.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
        crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Noticia+Text" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Martel+Sans" rel="stylesheet">

    <!--code en javascript------------------------------------------------->

    <script type="text/javascript" src="javascript/equipe.js"></script>


    <!--code en javascript------------------------------------------------->

</head>
<header role="navigation">

    <?php
            include('../partials/header.php');
             ?>

</header>

<body>

    <article class="banniere_equipe">
        <img src="images/banniere_equipe.jpg" alt="Image de tacos vegan" role="banner">
    </article>
    <div class="rectangle_vert">
        <p class="notre_equipe">Notre Equipe</p>
    </div>

    <article class="equipe" role="main">

        <p class="description">Nous vous fournissons une cuisine passionnée et de qualité depuis 1885.</p>

        <p class="presentation">Notre équipe est composée de : Laura, Claude et Marc.</p><br>


        <section class="photos_equipe">
            <!------Laura----------------------------->

            <div id="photo_laura" class="photo">
                <img src="images/image2_equipe.jpg" alt="Claude" title="Claude" onmouseover="display_laura()" onmouseout="hidden_laura()">
                <!--Se passe un truc quand je clique sur l'image-->
            </div>



            <!------Claude----------------------------->

            <div id="photo_claude" class="photo">
                <img src="images/image1_equipe.jpg" alt="Laura" title="Laura" onmouseover="display_claude()" onmouseout="hidden_claude()">

            </div>

            <!------Marc----------------------------->

            <div id="photo_marc" class="photo">
                <img src="images/image3_equipe.jpg" alt="Marc" title="Marc" onmouseover="display_marc()" onmouseout="hidden_marc()">
            </div>
        </section>

        <div id="rectangle_vert_description" class="rectangle_vert rectangle_description">
            <?php 
                while ($donnes=$result->fetchArray()) {
                    ?>
            <span id="<?php echo $donnes['id_description'];?>">
                <?php echo $donnes['description'];?></span>
            <?php
                }
                    ?>



        </div>



    </article>


    <footer role="contentinfo">
        <?php
                include('../partials/footer.php');
                 ?>
    </footer>
</body>

</html>