# Charte

## Répartition HTML
Tirage au sort pour la définition de la page de chacun:
* Actualités: Cédric
* Equipe: Romain
* Produits: Pascaline
* Contacts: Nacime

Chacun créé une page HTML pour sa partie et travaille dessus seul.

## Répartition CSS
2 Liens vers des feuilles de style + 1 pour les icones:

* 1 feuille CSS pour les parties communes du site (identiques sur chacunes des pages):
	* header
	* footer
	* structure de base du body (ce qui est commun aux 4 pages)

* 1 feuille de style CSS par page HTML.

* 1 lien vers le site fontawesome.com.

## Harmonisation du code
Ecrire tout en minuscules et sans accents.
Mettre des underscores pour les espaces.
Utiliser du français pour les id et classes.
Indenter correctement son code.
Utiliser des "" au moment opportun.

## Harmonisation des pratiques de travail en équipe
Travailler à partir du dossier "site".
Créer un sous dossier "images" dans le dossier "site" pour mettre les images de tout le monde.
Les images seront nommées selon le modèle: "fonction_page" ex: "banniere_produits".
Eviter les initiatives sur le travail commun.
Ne mettre sur la branche master que du "travail propre" et s'exercer sur la branche dev.

